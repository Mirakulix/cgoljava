package example.util;

import example.model.Game;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;


public class ParserTests {
    @BeforeEach
    public void setUp() throws Exception {

    }

    @Test
    public void testParseWithCells() throws IOException {
        // PREPARE
        Game game = new Game(3, 3);
        // clean gamefield
        for (int i = 0; i < game.field.length; i++) {
            for (int j = 0; j < game.field[0].length; j++) {
                game.setPoint(i, j, false, 0);
            }
        }
        URL resource = getClass().getClassLoader().getResource("testpattern/glider.cells");

        // TEST
        Parser.parse(game, resource.getPath());

        // ASSERT
        Print.field(game, 0);
        assertFalse(game.field[0][0]);
        assertTrue(game.field[0][1]);
        assertFalse(game.field[0][2]);

        assertFalse(game.field[1][0]);
        assertFalse(game.field[1][1]);
        assertTrue(game.field[1][2]);

        assertTrue(game.field[2][0]);
        assertTrue(game.field[2][1]);
        assertTrue(game.field[2][2]);
    }

    @Test
    public void testParseWithRle() throws IOException {
        // PREPARE
        Game game = new Game(3, 3);
        // clean gamefield
        for (int i = 0; i < game.field.length; i++) {
            for (int j = 0; j < game.field[0].length; j++) {
                game.setPoint(i, j, false, 0);
            }
        }
        URL resource = getClass().getClassLoader().getResource("testpattern/glider.rle");

        // TEST
        Parser.parse(game, resource.getPath());

        // ASSERT
        Print.field(game, 0);
        assertFalse(game.field[0][0]);
        assertTrue(game.field[0][1]);
        assertFalse(game.field[0][2]);

        assertFalse(game.field[1][0]);
        assertFalse(game.field[1][1]);
        assertTrue(game.field[1][2]);

        assertTrue(game.field[2][0]);
        assertTrue(game.field[2][1]);
        assertTrue(game.field[2][2]);
    }

    @Test
    public void testParseMissingSuffix() throws IOException {
        // PREPARE
        Game game = new Game(3, 3);
        // clean gamefield
        for (int i = 0; i < game.field.length; i++) {
            for (int j = 0; j < game.field[0].length; j++) {
                game.setPoint(i, j, false, 0);
            }
        }
        URL resource = getClass().getClassLoader().getResource("testpattern/glider");

        // TEST
        try {
            Parser.parse(game, resource.getPath());
            fail();

            // ASSERT
        } catch (FileNotFoundException e) {
//            assert that the exception got caught
            assertTrue(true);
        }
    }

    @Test
    public void testParseCouldNotReadFile() throws IOException {
        // PREPARE
        Game game = new Game(3, 3);
        // clean gamefield
        for (int i = 0; i < game.field.length; i++) {
            for (int j = 0; j < game.field[0].length; j++) {
                game.setPoint(i, j, false, 0);
            }
        }
        URL resource = getClass().getClassLoader().getResource("testpattern");

        // TEST
        try {
            Parser.parse(game, resource.getPath());
            fail();

            // ASSERT
        } catch (IOException e) {
//            assert that the exception got caught
            assertTrue(true);
        }
    }
}
