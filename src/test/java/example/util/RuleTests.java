package example.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RuleTests {

    @BeforeEach
    public void setUp() throws Exception {

    }

    @Test
    public void testRuleSuccess() {
        // PREPARE
        String ruleString = "b3/s23";

        // TEST
        Rule rule = new Rule(ruleString);

        // ASSERT
        assertEquals(1, rule.born.size());
        assertEquals(3, rule.born.get(0).intValue());

        assertEquals(2, rule.survive.size());
        assertEquals(2, rule.survive.get(0).intValue());
        assertEquals(3, rule.survive.get(1).intValue());
    }

    @Test
    public void testRuleValidationErrorMissingStartingB() {
        // PREPARE
        String brokenRuleString = "3/s23";

        // TEST
        Rule rule = new Rule(brokenRuleString);

        // ASSERT
        assertEquals(0, rule.survive.size());
        assertEquals(0, rule.born.size());
    }

    @Test
    public void testRuleValidationErrorMissingS() {
        // PREPARE
        String brokenRuleString = "b3/23";

        // TEST
        Rule rule = new Rule(brokenRuleString);

        // ASSERT
        assertEquals(0, rule.survive.size());
        assertEquals(0, rule.born.size());
    }

    @Test
    public void testRuleValidationErrorMissingSlash() {
        // PREPARE
        String brokenRuleString = "b3s23";

        // TEST
        Rule rule = new Rule(brokenRuleString);

        // ASSERT
        assertEquals(0, rule.survive.size());
        assertEquals(0, rule.born.size());
    }
}
