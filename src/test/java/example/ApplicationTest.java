package example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class ApplicationTest {

    @BeforeEach
    public void setUp() throws Exception {

    }

    @Disabled
    @Test
    public void applicationTest() {
        Application application = new Application();

        application.run();
    }
}
