package example.model;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

public final class Game {
    public final boolean[][] field;
    public final int[][] age;

    public Game(int lines, int columns) {
        field = new boolean[lines][columns];
        age = new int[lines][columns];
    }

    public void initField() {
        Random random;
        try {
            random = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            random = new Random();
        }

        for (int x = 0; x < field.length; x++) {
            for (int y = 0; y < field[0].length; y++) {
                field[x][y] = random.nextBoolean();
            }
        }
    }

    public void setPoint(int y, int x, boolean value, int ageValue) {
        field[y][x] = value;
        age[y][x] = ageValue;
    }
}
