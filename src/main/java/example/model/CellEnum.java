package example.model;


public enum CellEnum {
    ALIVE("\u2588"),
    DEAD(" "),
    BORDER("*");

    private final String value;

    CellEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
