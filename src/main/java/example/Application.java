package example;

import example.model.Game;
import example.util.Print;
import example.util.Rule;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.io.IOException;

import static example.util.Parser.parse;

@Command(name = "Conways game of life", mixinStandardHelpOptions = true, version = "1.0.0")
public class Application implements Runnable {


    @Option(names = "-c", description = "Columns (x axis)", arity = "1", defaultValue = "318")
    static int columns;

    @Option(names = "-r", description = "Rows (y axis)", arity = "1", defaultValue = "73")
    static int lines;

    @Option(names = "-i", description = "Amount of conway iterations", arity = "1", defaultValue = "1000")
    static int iterations;

    @Option(names = "-s", description = "Sleep (time between two renderings)", arity = "1", defaultValue = "16")  // ~ 60 hz
    static int sleep;

    @Option(names = "-p", description = "Start conway with a specific pattern", arity = "1", defaultValue = "")
    static String patternPath;

    @Option(names = "--rule", description = "Cellular automaton rule, e.g. B36/S23 for highlife.", arity = "1", defaultValue = "B3/S23")
    static String rule;

    public static void main(String[] args) {
        new CommandLine(new Application()).execute(args);
    }


    @Override
    public void run() {
        System.out.println("\u001B[2J");
        var game = new Game(lines, columns);

        if (patternPath.isBlank()) {
            game.initField();
        } else {
            try {
                parse(game, patternPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Rule currentRule = new Rule(Application.rule);

        for (int i = 0; i < iterations; i++) {
            Print.field(game, i);
            sleep(sleep);
            game = Conway.gameOfLive(currentRule, game);
        }
    }

    private void sleep(int timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }
}
