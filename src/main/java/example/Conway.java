package example;

import example.model.Game;
import example.util.Rule;

import java.util.List;
import java.util.Map;

public class Conway {

    public static Game gameOfLive(Rule rule, Game currentGeneration) {
        final int lines = currentGeneration.field.length;
        final int columns = currentGeneration.field[0].length;

        Game nextGeneration = new Game(lines, columns);
        for (int y = 0; y < lines; y++) {
            for (int x = 0; x < columns; x++) {
                if (currentGeneration.field[y][x]) {
                    nextGeneration.setPoint(y, x, applyRules(rule, currentGeneration, y, x), currentGeneration.age[y][x]);
                } else {
                    nextGeneration.setPoint(y, x, applyRules(rule, currentGeneration, y, x), 0);
                }
            }
        }
        return nextGeneration;
    }

    private static boolean applyRules(Rule rule, Game game, int y, int x) {
        int neighbours = countNeighbours(game.field, y, x);

        if (game.field[y][x]) {
            game.age[y][x] = rule.survive.contains(neighbours)
                    ? game.age[y][x] += 1
                    : 0;
            return rule.survive.contains(neighbours);
        } else {
            return rule.born.contains(neighbours);
        }
    }

    private static int countNeighbours(boolean[][] field, final int y, final int x) {
        final int lines = field.length;
        final int columns = field[0].length;


        final int yTop = getNeighbourPosition(y, 1, lines);
        final int yBot = getNeighbourPosition(y, -1, lines);
        final int xRight = getNeighbourPosition(x, 1, columns);
        final int xLeft = getNeighbourPosition(x, -1, columns);

        // neighbours
        return (int) List.of
                (
                        Map.entry(yBot, xLeft), Map.entry(yBot, x), Map.entry(yBot, xRight),
                        Map.entry(y, xLeft)                       , Map.entry(y, xRight),
                        Map.entry(yTop, xLeft), Map.entry(yTop, x), Map.entry(yTop, xRight)
                )
                .stream()
                .map(pos -> field[pos.getKey()][pos.getValue()])
                .filter(i -> i)
                .count();
    }

    private static int getNeighbourPosition(int pos, int delta, int limit) {
        return Math.floorMod((pos + delta), limit);
    }
}