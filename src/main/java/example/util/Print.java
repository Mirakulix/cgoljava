package example.util;

import example.model.CellEnum;
import example.model.Game;

public class Print {

    private Print() {
    }

    public static void field(Game game, int iteration) {
        StringBuilder column = new StringBuilder();
        StringBuilder columns = new StringBuilder();
        columns.append("\033[H\r");
        for (int y = 0; y < game.field.length; y++) {
            for (int x = 0; x < game.field[y].length; x++) {
                if (game.field[y][x]) {
                    column.append(colourMap(game.age[y][x])).append(CellEnum.ALIVE.getValue()).append("\u001B[0m");
                } else {
                    column.append(CellEnum.DEAD.getValue());
                }
            }
            columns.append(column.toString()).append("\n");
            column.delete(0, column.length());
        }

        columns.append("_".repeat(game.field[0].length));
        columns.append("Iteration: ").append(iteration);
        columns.append(" Length: ").append(game.field.length);
        columns.append(" Width: ").append(game.field[0].length);
        System.out.print(columns.toString());
    }

    private static String colourMap(int age) {
        switch (age) {
            case 0:
                return "\u001B[38;5;34m";
            case 1:
                return "\u001B[38;5;35m";
            case 2:
                return "\u001B[38;5;36m";
            case 3:
                return "\u001B[38;5;37m";
            case 4:
                return "\u001B[38;5;38m";
            case 5:
                return "\u001B[38;5;39m";
            default:
                return "\u001B[38;5;21m";
        }
    }
}
