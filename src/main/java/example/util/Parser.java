package example.util;

import example.model.Game;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Parser {

    private static final String CELLS_SUFFIX = ".cells";
    private static final String RLE_SUFFIX = ".rle";
    private static final char DEAD_CELL = '.';
    private static final char ALIVE_CELL = '0';
    private static final char DEAD_RLE = 'b';
    private static final char ALIVE_RLE = 'o';


    private Parser() {

    }

    public static void parse(Game game, String filePath) throws IOException {
        final List<String> file = Files.readAllLines(Paths.get(filePath));

        List<String> fileContent = getFileContent(file, filePath);
        if (!fileContent.isEmpty()) {
            if (filePath.endsWith(CELLS_SUFFIX)) {
                parseCells(game, fileContent);
            }
            if (filePath.endsWith(RLE_SUFFIX)) {
                parseRle(game, fileContent);
            }
        } else {
            throw new FileNotFoundException("Missing or unknown filetype suffix");
        }
    }

    private static List<String> getFileContent(final List<String> file, final String filepath) {
        final List<String> fileContent;
        if (filepath.endsWith(CELLS_SUFFIX)) {
            fileContent = file.stream()
                    .filter(f -> !f.startsWith("!"))
                    .collect(Collectors.toList());
        } else if (filepath.endsWith(RLE_SUFFIX)) {
            Optional<String> optionalFileContent = file.stream()
                    .filter(f -> !f.startsWith("#"))
                    .filter(f -> !f.startsWith("x"))
                    .filter(f -> !f.startsWith("y"))
                    .filter(f -> !f.startsWith("rule"))
                    .findFirst();

            fileContent = optionalFileContent.map(s -> List.of(s.split("\\$"))).orElseGet(List::of);
        } else {
            // unkown suffix
            fileContent = List.of();
        }

        return fileContent;
    }

    private static void parseCells(Game game, List<String> cellsFile) {
        int x = 0;
        int y = 0;
        for (String line : cellsFile) {
            for (int i = 0; i < line.length(); i++) {
                game.setPoint(y, x, line.charAt(i) != DEAD_CELL, 0);
                x++;
            }
            x = 0;
            y++;
        }
    }

    private static void parseRle(Game game, List<String> rleFile) {
        List<String> cellsFile = rleToCells(rleFile);

        parseCells(game, cellsFile);
    }

    private static List<String> rleToCells(List<String> rleFile) {
        StringBuilder repeater = new StringBuilder();
        List<String> cellsFile = new ArrayList<>();
        StringBuilder line = new StringBuilder();
        for (String rleLine : rleFile) {
            for (int i = 0; i < rleLine.length(); i++) {
                switch (rleLine.charAt(i)) {
                    case DEAD_RLE:
                        appendToLine(repeater, line, DEAD_CELL);
                        break;
                    case ALIVE_RLE:
                        appendToLine(repeater, line, ALIVE_CELL);
                        break;
                    default:
                        repeater.append(rleLine.charAt(i));
                        break;
                }
            }

            cellsFile.add(line.toString());
            line.replace(0, line.length(), "");
        }

        return cellsFile;
    }

    private static void appendToLine(StringBuilder repeater, StringBuilder line, char aliveCell) {
        if (repeater.toString().isBlank()) {
            line.append(aliveCell);
        } else {
            line.append(String.valueOf(aliveCell).repeat(Integer.parseInt(repeater.toString())));
            repeater.replace(0, repeater.length(), "");
        }
    }
}
