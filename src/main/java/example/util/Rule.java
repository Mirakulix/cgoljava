package example.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class Rule {
    public final List<Integer> survive = new ArrayList<>();
    public final List<Integer> born = new ArrayList<>();


    public Rule(String rule) {

        rule = rule.toLowerCase();
        rule = rule.trim();
        // validation
        if (rule.contains("/")) {
            String[] x = rule.split("/");
            if (x[0].startsWith("b") && x[1].startsWith("s")) {
                x[0].chars()
                        .skip(1)
                        .mapToObj(i -> (char) i)
                        .forEach(c -> born.add(Integer.parseInt(Character.toString(c))));
                x[1].chars()
                        .skip(1)
                        .mapToObj(i -> (char) i)
                        .forEach(c -> survive.add(Integer.parseInt(Character.toString(c))));
            } else {
                System.err.print("Wrong format, pls type \"b/s numbers / s/b numbers\"");
            }
        }
    }
}
